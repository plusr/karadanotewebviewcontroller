//
//  KNPWebViewController.m
//  KaradanoteWebViewController
//
//  Created by akuraru on 10/30/2014.
//  Copyright (c) 2014 akuraru. All rights reserved.
//

#import <Masonry/Masonry.h>
#import <GoogleAnalytics/GAI.h>
#import <GoogleAnalytics/GAIFields.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import <CrayWebViewController/CrayNavigationController.h>
#import "KNPWebViewController.h"
#import "KNPLogin.h"
#import "WebViewIntegrator.h"
#import "NJKWebViewProgressView.h"
#import "KNPBookmark.h"
#import "TUSafariActivity.h"
#import "Firebase.h"

@implementation KNPCachedResult
- (instancetype)initWithNavigation:(UINavigationController *)navigation webViewController:(KNPWebViewController *)webViewController cached:(BOOL)cached {
    self = [super init];
    if (self) {
        _navigation = navigation;
        _webViewController = webViewController;
        _cached = cached;
    }
    return self;
}
@end


@interface CrayTabWebViewController ()
@property(nonatomic, strong) NJKWebViewProgressView *progressView;
@property(weak, nonatomic) IBOutlet UIWebView *contentWebView;
@end

@interface KNPWebViewController ()
@property(nonatomic, strong) UIButton *home;
@property(nonatomic, strong) UIButton *back;
@property(nonatomic, strong) UIButton *forward;
@property(nonatomic, strong) UIButton *otherApp;

@property(nonatomic, strong) KNPLogin *login;
@property(nonatomic, strong) KNPBookmark *bookmark;
@property(nonatomic, strong) WebViewIntegrator *integrator;
@property(nonatomic, strong) NSString *stringData;

@property(nonatomic, strong) void (^preloadComplete)();
@property(nonatomic, strong) UIButton *navigationViewBack;
@property(nonatomic, strong) UIButton *navigationViewForward;
@end

@implementation KNPWebViewController
+ (NSMutableDictionary *)navigationControllers {
    static NSMutableDictionary *navigationControllers;
    if (navigationControllers == nil) {
        navigationControllers = [NSMutableDictionary dictionary];
    }
    return navigationControllers;
}

+ (instancetype)webViewController {
    return [[self alloc] init];
}

+ (KNPCachedResult *)navigationControllerWithURL:(NSString *)URL cache:(BOOL)on {
    NSMutableDictionary *navigationControllers = [self navigationControllers];
    UINavigationController *this = (on) ? navigationControllers[URL] : nil;
    BOOL cached = this != nil;
    
    KNPWebViewController *webViewController;
    if (cached == NO) {
         webViewController = [self webViewController];
        webViewController.URL = URL;
        webViewController.navigationBarHidden = YES;
        this = [[CrayNavigationController alloc] initWithRootViewController:webViewController];
        if (on) {
            navigationControllers[URL] = this;
        }
    } else {
        webViewController = (KNPWebViewController *)this.topViewController;
    }
    return [[KNPCachedResult alloc] initWithNavigation:this webViewController:webViewController cached:cached];
}

+ (instancetype)showFromViewController:(UIViewController *)controller URL:(NSString *)URL {
    return [self showFromViewController:controller URL:URL cache:NO];
}

+ (instancetype)showFromViewController:(UIViewController *)controller URL:(NSString *)URL cache:(BOOL)on {
    return [self showFromViewController:controller URL:URL cache:on completion:nil];
}

+ (instancetype)showFromViewController:(UIViewController *)controller URL:(NSString *)URL cache:(BOOL)on completion:(void (^)(KNPWebViewController *controller, BOOL cached))completion {
    KNPCachedResult *result = [self navigationControllerWithURL:URL cache:on];
    KNPWebViewController *web = result.webViewController;
    [web show:result controller:controller completion:completion];
    return web;
}

+ (void)preload:(NSArray *)URLs viewController:(UIWindow *)window {
    NSString *URL = URLs.firstObject;
    if (URL) {
        UIViewController *root = window.rootViewController;
        KNPCachedResult *result = [self navigationControllerWithURL:URL cache:YES];
        UINavigationController *navigation = [result navigation];
        __weak KNPWebViewController *content = (KNPWebViewController *) navigation.topViewController;
        content.preloadComplete = ^{
            window.rootViewController = root;
            [self preload:[URLs subarrayWithRange:NSMakeRange(1, URLs.count -1)] viewController:window];
            content.preloadComplete = nil;
        };
        window.rootViewController = navigation;
    }
}

- (void)show:(KNPCachedResult *)result controller:(UIViewController *)controller completion:(void (^)(KNPWebViewController *controller, BOOL cached))completion {
    NSURL *requestURL = [NSURL URLWithString:self.URL];
    if ([[self class] shouldNotStartLoadWithRequest:requestURL]) {
        [[UIApplication sharedApplication] openURL:requestURL];
        if (completion) {
            completion(nil, NO);
        }
    } else {
        [controller presentViewController:result.navigation animated:YES completion:^(){
            if (completion) {
                completion(result.webViewController, result.cached);
            }
        }];
    }
}

- (UIView *)currentView {
    return self.view;
}

- (void)viewDidLoad {
    [self createView];
    [super viewDidLoad];

    self.automaticallyAdjustsScrollViewInsets = NO;
    self.login = [[KNPLogin alloc] init];
    self.bookmark = [[KNPBookmark alloc] init];
    self.integrator = [WebViewIntegrator integratorWithWebView:self.contentWebView];
    self.navigationItem.title = self.titleText;
    if (self.closeButtonImage) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:self.closeButtonImage style:UIBarButtonItemStylePlain target:self action:@selector(closeView:)];
    }
    self.navigationController.navigationBarHidden = self.navigationBarHidden;
    self.navigationController.toolbarHidden = (self.preloadComplete != nil);
    self.navigationController.toolbar.translucent = NO;
    if (self.navigationViewShow) {
        self.navigationController.toolbarHidden = YES;
        [self createNavigationView];
    }
    UIColor *barColor = [UIColor colorWithRed:255 / 255.0 green:253 / 255.0 blue:238 / 255.0 alpha:1.0];
    if ([self over7]) {
        self.navigationController.toolbar.barTintColor = barColor;
    } else {
        self.navigationController.toolbar.tintColor = barColor;
    }
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.contentWebView.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.contentWebView addSubview:self.progressView];
    [self.progressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentWebView);
        make.right.equalTo(self.contentWebView);
        make.top.equalTo(self.mas_topLayoutGuide);
        make.height.equalTo(@2.5);
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.preloadComplete) {
        self.navigationController.toolbarHidden = NO;
        self.preloadComplete();
    }
    if (self.navigationViewShow) {
        if (self.navigationView) {
            self.navigationView.frame = CGRectMake(self.contentWebView.frame.size.width - 100.0, self.contentWebView.frame.size.height - 60.0, 92.0, 32.0);
            self.navigationView.alpha = 0.9;
        }
    }
    if (self.dialogView) {
        self.dialogView.alpha = 0.0;
        if (self.navigationBarHidden) {
            [self.view addSubview:self.dialogView];
        } else {
            [self.navigationController.view addSubview:self.dialogView];
        }
        
        [UIView animateWithDuration:1.5 delay:0.0 usingSpringWithDamping:0.5 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.dialogView.alpha = 1.0;
        } completion:nil];
    }
}

- (void)createView {
    UIView *currentView = [self currentView];
    self.contentWebView = [self createWebViewWithSuperview:currentView];
    
    self.toolbarItems = [self toolbarItems];
}

- (void)createNavigationView {
    self.navigationView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 92.0, 32.0)];
    [self.navigationView setBackgroundColor:[[UIColor alloc] initWithRed:0.95 green:0.95 blue:0.95 alpha:0.9]];
    self.navigationView.layer.cornerRadius = 16.0;
    self.navigationView.alpha = 0.0;
    [self.view addSubview:self.navigationView];
    
    self.navigationViewBack = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 42.0, 32.0)];
    
    [self.navigationViewBack setImage:self.backButtonActiveImage forState:UIControlStateNormal];
    [self.navigationViewBack setImage:self.backButtonImage forState:UIControlStateHighlighted];
    [self.navigationViewBack setImage:self.backButtonImage forState:UIControlStateDisabled];
    [self.navigationViewBack addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationViewBack setEnabled:NO];
    [self.navigationView addSubview:self.navigationViewBack];
    
    self.navigationViewForward = [[UIButton alloc] initWithFrame:CGRectMake(50.0, 0.0, 42.0, 32.0)];
    [self.navigationViewForward setImage:self.forwardButtonActiveImage forState:UIControlStateNormal];
    [self.navigationViewForward setImage:self.forwardButtonImage forState:UIControlStateHighlighted];
    [self.navigationViewForward setImage:self.forwardButtonImage forState:UIControlStateDisabled];
    [self.navigationViewForward addTarget:self action:@selector(forward:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationViewForward setEnabled:NO];
    [self.navigationView addSubview:self.navigationViewForward];
    
//    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragNavigationView:)];
//    [self.navigationView addGestureRecognizer:panGesture];
}

- (UIWebView *)createWebViewWithSuperview:(__weak UIView *)superview {
    UIWebView *webView = [[UIWebView alloc] init];
    webView.scalesPageToFit = YES;

    [superview addSubview:webView];

    [webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_topLayoutGuide);
        make.left.equalTo(superview);
        make.right.equalTo(superview);
        make.bottom.equalTo(self.mas_bottomLayoutGuide);
    }];
    
    if (self.adView) {
        [superview addSubview:self.adView];
        [self.adView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(superview);
            make.left.equalTo(superview);
            make.right.equalTo(superview);
            make.bottom.equalTo(self.mas_bottomLayoutGuide);
        }];
    }
    return webView;
}

- (NSArray *)toolbarItems {
    id home = [self createBar:@"browser_backapp" action:@selector(closeView:)];
    id back = [self createBar:@"browser_back" action:@selector(back:)];
    id forward = [self createBar:@"browser_go" action:@selector(forward:)];
    id otherApp = [self createBar:@"app_download_button" action:@selector(openOtherApplication:)];

    UIBarButtonItem *fixspaser = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    NSArray *items = @[home, fixspaser, back, fixspaser, forward, fixspaser, otherApp];

    self.home = (UIButton *) [home customView];
    [self.home setImage:[self activeImageForNamed:@"browser_backapp"] forState:UIControlStateNormal];
    self.back = (UIButton *) [back customView];
    self.forward = (UIButton *) [forward customView];
    self.otherApp = (UIButton *) [otherApp customView];
    return items;
}

- (UIBarButtonItem *)createBar:(NSString *)imageName action:(SEL)action {
    UIImage *image = [self imageForNamed:imageName];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [button setImage:image forState:UIControlStateNormal];
    UIImage *highlighted = [self activeImageForNamed:imageName];
    [button setImage:highlighted forState:UIControlStateHighlighted];
    [button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (UIImage *)activeImageForNamed:(NSString *)named {
    return [self imageForNamed:[named stringByAppendingString:@"_active"]];
}

- (UIImage *)imageForNamed:(NSString *)named {
    NSString *imagePath = [[self bundle] pathForResource:named ofType:@"png"];
    return [UIImage imageWithContentsOfFile:imagePath];
}

- (void)dragNavigationView:(id)sender {
    UIView *draggedView = [sender view];
    CGPoint point = [sender translationInView:draggedView];
    CGFloat nextX = draggedView.center.x + point.x;
    if (nextX < 36.0) {
        nextX = 36.0;
    } else if (nextX > self.view.frame.size.width - 36.0) {
        nextX = self.view.frame.size.width - 36.0;
    }
    CGFloat nextY = draggedView.center.y + point.y;
    if (nextY < 16.0 + self.contentWebView.frame.origin.y) {
        nextY = 16.0 + self.contentWebView.frame.origin.y;
    } else if (nextY > self.view.frame.size.height - 16.0) {
        nextY = self.view.frame.size.height - 16.0;
    }
    draggedView.center = CGPointMake(nextX, nextY);
    [sender setTranslation:CGPointZero inView:self.view];
}

- (void)closeView:(id)sender {
    [self dismissViewControllerAnimated:true completion:self.dismiss];
}

- (void)favorite:(id)sender {
    [self.contentWebView stringByEvaluatingJavaScriptFromString:@"articleBookmark().done(function(result){}).fail(function(result){});"];
    if (self.login.status == KNPLoginStatusDone) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[self switchTitle:self.bookmark.status] message:nil preferredStyle:UIAlertControllerStyleAlert];
        [self presentViewController:alertController animated:YES completion:nil];
        
        dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC / 2);
        dispatch_after(time, dispatch_get_main_queue(), ^{
            [self checkBookmark];
            [alertController dismissViewControllerAnimated:YES completion:nil];
        });
    }
}

- (void)sendTrack {
    NSString *screenName = self.currentPageURL;
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[screenName copy]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

    [FIRAnalytics setScreenName:screenName screenClass:@"KaradanoteWebViewController"];
}

- (NSString *)switchTitle:(KNPBookmarkStatus)status {
    switch (status) {
        case KNPBookmarkStatusDone:
            return @"ブックマークから外しました";
        case KNPBookmarkStatusUntil:
            return @"ブックマークに追加しました";
        default:
            return @"";
    }
}

- (void)back:(id)sender {
    if ([self.contentWebView canGoBack]) {
        [self.contentWebView goBack];
    }
}

- (void)forward:(id)forward {
    if ([self.contentWebView canGoForward]) {
        [self.contentWebView goForward];
    }
}

- (void)openOtherApplication:(id)sender {
    NSURL *url = [self contentWebView].request.URL ?: [[NSURL alloc] init];
    UIActivityViewController *activities = [[UIActivityViewController alloc] initWithActivityItems:@[url] applicationActivities:@[[[TUSafariActivity alloc] init]]];
    [self presentViewController:activities animated:YES completion:nil];

    if ([self over8] && [self isIpad]) {
        UIPopoverPresentationController *presentationController = [activities popoverPresentationController];
        presentationController.sourceView = self.otherApp;
    }
}

- (BOOL)isIpad {
    return UIDevice.currentDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad;
}

- (BOOL)over8 {
    return NSFoundationVersionNumber_iOS_7_1 < NSFoundationVersionNumber;
}

- (NSBundle *)bundle {
    if (self.alwaysUseMainBundle) {
        return [NSBundle mainBundle];
    } else {
        NSURL *bundleURL = [[NSBundle mainBundle] URLForResource:@"KaradanoteWebViewController" withExtension:@"bundle"];
        if (bundleURL) {
            // AAMFeedback.bundle will likely only exist when used via CocoaPods
            return [NSBundle bundleWithURL:bundleURL];
        } else {
            return [NSBundle mainBundle];
        }
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [super webViewDidFinishLoad:webView];

    [self checkLogin];
    [self checkBookmark];

    [self updateBackButton];
    [self updateForwardButton];
    
    if ([self.delegate respondsToSelector:@selector(resultLoadFinishScript:)]) {
        NSString *string = [webView stringByEvaluatingJavaScriptFromString:self.javascript];
        [self.delegate resultLoadFinishScript:string];
    }
    
    if ([self.adView respondsToSelector:@selector(webViewDidFinishLoad:)]) {
        [self.adView webViewDidFinishLoad:webView];
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    if ([self.adView respondsToSelector:@selector(webViewDidStartLoad:)]) {
        [self.adView webViewDidStartLoad:webView];
    }
}

- (void)checkLogin {
    NSString *login = [self.contentWebView stringByEvaluatingJavaScriptFromString:@"document.getElementById('login_status').getAttribute('status');"];
    [self.login setParameter:login];
}

- (void)checkBookmark {
    NSString *bookmark = [self.contentWebView stringByEvaluatingJavaScriptFromString:@"document.getElementById('article_bookrmark_status').getAttribute('status')"];
    [self.bookmark setParameter:bookmark];
}

- (void)updateBackButton {
    NSLog(@"%@", NSStringFromCGRect(self.contentWebView.frame));
    NSLog(@"%@", NSStringFromCGRect([[UIApplication sharedApplication] statusBarFrame]));
    NSLog(@"%@", NSStringFromCGRect(self.navigationController.navigationBar.frame));
    
    self.back.userInteractionEnabled = self.contentWebView.canGoBack;
    self.back.highlighted = self.contentWebView.canGoBack;
    if (self.navigationViewShow) {
        [self.navigationViewBack setEnabled:self.contentWebView.canGoBack];
    }
}

- (void)updateForwardButton {
    self.forward.userInteractionEnabled = self.contentWebView.canGoForward;
    self.forward.highlighted = self.contentWebView.canGoForward;
    if (self.navigationViewShow) {
        [self.navigationViewForward setEnabled:self.contentWebView.canGoForward];
    }
}

- (BOOL)over7 {
    return NSFoundationVersionNumber_iOS_6_1 < NSFoundationVersionNumber;
}

- (void)goToAddress:(NSString *)URL {
    if (URL == nil) {
        return;
    }
    NSURL *url = [NSURL URLWithString:URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [self addHeader:request];
    [self.contentWebView loadRequest:request];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([request isKindOfClass:[NSMutableURLRequest class]]) {
        NSMutableURLRequest *r = (NSMutableURLRequest *)request;
        [self addHeader:r];
        return [self webView:webView shouldStartLoadWithRequest:r];
    }
    if (navigationType != UIWebViewNavigationTypeLinkClicked &&
        navigationType != UIWebViewNavigationTypeFormSubmitted) {
        return [self webView:webView shouldStartLoadWithRequest:request];
    }

    NSDictionary *headers = [request allHTTPHeaderFields];
    BOOL hasHeader = headers[@"os"] != nil;
    if (hasHeader) {
        return [self webView:webView shouldStartLoadWithRequest:request];
    }

    // relaunch with a modified request
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            NSMutableURLRequest *r = [request mutableCopy];
            [self addHeader:r];
            [webView loadRequest:r];
        });
    });
    return NO;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request {
    NSURL *requestURL = request.URL;
    if ([self.class shouldNotStartLoadWithRequest:requestURL]) {
        if ([webView canGoBack] == NO) {
            [self dismissViewControllerAnimated:NO completion:nil];
        }
        [[UIApplication sharedApplication] openURL:requestURL];
        return NO;
    }
    return YES;
}

+ (BOOL)shouldNotStartLoadWithRequest:(NSURL *)requestURL {
    NSString *scheme = requestURL.scheme;
    return (![scheme isEqual:@"http"] && ![scheme isEqual:@"https"] && ![scheme isEqual:@"file"]) &&
            ([[UIApplication sharedApplication] canOpenURL:requestURL]);
}

- (void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress {
    [super webViewProgress:webViewProgress updateProgress:progress];
//    [self.progressView setProgress:progress animated:YES];
    if (progress == 1.0) {
        [self completeProgress];
    }
}

- (void)completeProgress {
    [self sendTrack];
}

- (void)addHeader:(NSMutableURLRequest *)request {
    [request addValue:@"ios" forHTTPHeaderField:@"os"];
    [request addValue:[self _appVersion] forHTTPHeaderField:@"version"];
    [request addValue:[self shortName] forHTTPHeaderField:@"app_name"];
    [request addValue:[UIDevice currentDevice].identifierForVendor.UUIDString forHTTPHeaderField:@"uuid"];
    [request addValue:self.stringData forHTTPHeaderField:@"data"];
}

- (void)setData:(NSDictionary *)data {
    _data = data;
    _stringData = nil;
}

- (NSString *)stringData {
    if (_stringData == nil) {
        if (self.data) {
            _stringData = [self dateForDictionary:self.data];
        }
    }
    return _stringData;
}

- (NSString *)_appVersion {
    return [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"];
}

- (NSString *)shortName {
    return [[NSBundle mainBundle] infoDictionary][@"Short Name"];
}

- (NSString *)dateForDictionary:(NSDictionary *)dictionary {
    NSString *result = @"";
    for (NSString *key in dictionary.allKeys) {
        result = [result stringByAppendingFormat:@"%@=%@,", key, dictionary[key]];
    }
    return (result.length == 0) ? nil : [result substringToIndex:result.length - 1];
}

- (void)dealloc {
    self.contentWebView.delegate = nil;
}
@end
