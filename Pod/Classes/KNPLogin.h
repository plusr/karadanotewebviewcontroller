//
//  KNPLoginStatus.h
//  KaradanoteWebViewController
//
//  Created by akuraru on 10/30/2014.
//  Copyright (c) 2014 akuraru. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, KNPLoginStatus) {
    KNPLoginStatusDone,
    KNPLoginStatusUntil,
};

@interface KNPLogin: NSObject
@property (readonly, nonatomic) KNPLoginStatus status;

- (void)setParameter:(NSString *)parameter;
@end