//
//  KNPLoginStatus.m
//  KaradanoteWebViewController
//
//  Created by akuraru on 10/30/2014.
//  Copyright (c) 2014 akuraru. All rights reserved.
//


#import "KNPLogin.h"

@interface KNPLogin ()
@property (nonatomic) KNPLoginStatus status;
@end

@implementation KNPLogin
- (instancetype)init {
    self = [super init];
    if (self) {
        self.status = KNPLoginStatusUntil;
    }
    return self;
}

- (void)setParameter:(NSString *)parameter {
    if ([parameter isEqualToString:@"1"]) {
        self.status = KNPLoginStatusDone;
    } else if ([parameter isEqualToString:@"2"]) {
        self.status = KNPLoginStatusUntil;
    }
}
@end