//
//  KNPLoginStatus.m
//  KaradanoteWebViewController
//
//  Created by akuraru on 10/30/2014.
//  Copyright (c) 2014 akuraru. All rights reserved.
//


#import "KNPBookmark.h"

@interface KNPBookmark ()
@property (nonatomic) KNPBookmarkStatus status;
@end

@implementation KNPBookmark
- (instancetype)init {
    self = [super init];
    if (self) {
        self.status = KNPBookmarkStatusUnknow;
    }
    return self;
}

- (void)setParameter:(NSString *)parameter {
    if ([parameter isEqualToString:@"1"]) {
        self.status = KNPBookmarkStatusDone;
    } else if ([parameter isEqualToString:@"2"]) {
        self.status = KNPBookmarkStatusUntil;
    } else {
        self.status = KNPBookmarkStatusUnknow;
    }
}
@end