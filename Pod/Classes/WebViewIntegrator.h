//
// Created by azu on 2014/02/05.
//


#import <Foundation/Foundation.h>

@class WebViewJavascriptBridge;

/* @require
    pod 'WebViewJavascriptBridge', '~> 4.1.0'
    pod 'iOSInstalledApps', '~> 0.2.0'
 */
@interface WebViewIntegrator : NSObject

+ (instancetype)integratorWithWebView:(UIWebView *) webView webViewDelegate:(NSObject <UIWebViewDelegate> *) webViewDelegate;
+ (instancetype)integratorWithWebView:(UIWebView *) webView;

@end
