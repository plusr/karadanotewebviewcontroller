//
//  KNPWebViewController.h
//  KaradanoteWebViewController
//
//  Created by akuraru on 10/30/2014.
//  Copyright (c) 2014 akuraru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CrayWebViewController/CrayTabWebViewController.h>

@class KNPWebViewController;

@interface KNPCachedResult : NSObject
@property(readonly, strong, nonatomic) UINavigationController *navigation;
@property(readonly, strong, nonatomic) KNPWebViewController *webViewController;
@property(readonly, nonatomic) BOOL cached;
@end

@protocol KNPWebViewControllerDelegate <NSObject>
@optional
- (void)resultLoadFinishScript: (NSString *)result;
@end

@interface KNPWebViewController : CrayTabWebViewController

@property(nonatomic) BOOL navigationBarHidden; // defaults YES
@property(nonatomic) BOOL alwaysUseMainBundle;
@property(nonatomic) BOOL navigationViewShow;
@property(nonatomic) NSString *titleText;
@property(nonatomic) UIView *navigationView;
@property(nonatomic) UIImage *closeButtonImage;
@property(nonatomic) UIImage *backButtonActiveImage;
@property(nonatomic) UIImage *backButtonImage;
@property(nonatomic) UIImage *forwardButtonActiveImage;
@property(nonatomic) UIImage *forwardButtonImage;

@property(strong, nonatomic) NSDictionary *data;
@property(strong, nonatomic) UIView<UIWebViewDelegate> *adView;
@property(strong, nonatomic) UIView *dialogView;
@property (weak, nonatomic) id <KNPWebViewControllerDelegate> delegate;
@property(nonatomic) NSString *javascript;
@property(copy, nonatomic) void (^dismiss)(void);

+ (instancetype)webViewController;

+ (KNPCachedResult *)navigationControllerWithURL:(NSString *)URL cache:(BOOL)on;

+ (instancetype)showFromViewController:(UIViewController *)controller URL:(NSString *)URL;

// cacheをYESにすると前回開いたところから始まります
+ (instancetype)showFromViewController:(UIViewController *)controller URL:(NSString *)URL cache:(BOOL)on;

+ (instancetype)showFromViewController:(UIViewController *)controller URL:(NSString *)URL cache:(BOOL)on completion:(void (^)(KNPWebViewController *controller, BOOL cached))completion;

- (void)show:(KNPCachedResult *)result controller:(UIViewController *)controller completion:(void (^)(KNPWebViewController *controller, BOOL cached))completion;

// 事前にページをロードします。
// application:didFinishLaunchingWithOptions: で行ってください。
+ (void)preload:(NSArray *)URLs viewController:(UIWindow *)window;
@end
