//
// Created by azu on 2014/02/05.
//


#import <WebViewJavascriptBridge/WebViewJavascriptBridge.h>
#import <LLInstalledApps.h>
#import "WebViewIntegrator.h"


@interface WebViewIntegrator ()
@property(nonatomic, strong) WebViewJavascriptBridge *bridge;
@property(nonatomic, strong) UIWebView *webView;
@property(nonatomic, strong) NSObject <UIWebViewDelegate> *webViewDelegate;
@end

@implementation WebViewIntegrator {

}
- (instancetype)initWithWebView:(UIWebView *) webView webViewDelegate:(NSObject <UIWebViewDelegate> *) webViewDelegate {
    self = [super init];
    if (self == nil) {
        return nil;
    }

    self.webView = webView;
    self.webViewDelegate = webViewDelegate;
    [WebViewJavascriptBridge enableLogging];
    [self integrationInstallHandler];
    return self;
}

+ (instancetype)integratorWithWebView:(UIWebView *) webView webViewDelegate:(NSObject <UIWebViewDelegate> *) webViewDelegate {
    return [[self alloc] initWithWebView:webView webViewDelegate:webViewDelegate];
}
+ (instancetype)integratorWithWebView:(UIWebView *) webView {
    return [[self alloc] initWithWebView:webView webViewDelegate:webView.delegate];
}

- (void)integrationInstallHandler {
    self.bridge = [WebViewJavascriptBridge bridgeForWebView:self.webView];
    [self.bridge setWebViewDelegate:self.webViewDelegate];
    __weak typeof (self) that = self;
    [self.bridge registerHandler:@"isInstalledApp" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSString *url = [self.webView stringByEvaluatingJavaScriptFromString:@"window.location.href"];
        if ([that isKaradanoteURL:url]) {
            NSArray *installAppList = data;
            NSDictionary *installAppDictionary = [LLInstalledApps appsInstalledWithSchemes:installAppList];
            responseCallback(installAppDictionary);
        }
    }];
}

// かなり緩いチェック
- (BOOL)isKaradanoteURL:(NSString *) urlString {
    return [urlString rangeOfString:@"karadanote.jp"].location != NSNotFound;
}
@end