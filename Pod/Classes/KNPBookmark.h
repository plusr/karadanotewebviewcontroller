//
//  KNPLoginStatus.h
//  KaradanoteWebViewController
//
//  Created by akuraru on 10/30/2014.
//  Copyright (c) 2014 akuraru. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, KNPBookmarkStatus) {
    KNPBookmarkStatusDone,
    KNPBookmarkStatusUntil,
    KNPBookmarkStatusUnknow,
};

@interface KNPBookmark: NSObject
@property (readonly, nonatomic) KNPBookmarkStatus status;

- (void)setParameter:(NSString *)parameter;
@end