# KaradanoteWebViewController

[![CI Status](http://img.shields.io/travis/akuraru/KaradanoteWebViewController.svg?style=flat)](https://travis-ci.org/akuraru/KaradanoteWebViewController)
[![Version](https://img.shields.io/cocoapods/v/KaradanoteWebViewController.svg?style=flat)](http://cocoadocs.org/docsets/KaradanoteWebViewController)
[![License](https://img.shields.io/cocoapods/l/KaradanoteWebViewController.svg?style=flat)](http://cocoadocs.org/docsets/KaradanoteWebViewController)
[![Platform](https://img.shields.io/cocoapods/p/KaradanoteWebViewController.svg?style=flat)](http://cocoadocs.org/docsets/KaradanoteWebViewController)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KaradanoteWebViewController is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "KaradanoteWebViewController"

## Author

akuraru, akuraru@gmail.com

## License

KaradanoteWebViewController is available under the MIT license. See the LICENSE file for more info.

