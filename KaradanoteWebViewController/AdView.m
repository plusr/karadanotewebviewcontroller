//
//  AdView.m
//  KaradanoteWebViewController
//
//  Created by hakozaki on 2015/10/28.
//  Copyright © 2015年 akuraru. All rights reserved.
//

#import "AdView.h"

@implementation AdView

- (void)webViewDidStartLoad:(UIWebView *)webView{
    self.loadLabel.text = @"ロード中です";
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    if ([[webView stringByEvaluatingJavaScriptFromString:@"document.readyState"] isEqualToString:@"complete"]) {
        self.closeButton.hidden = NO;
        self.loadLabel.text = @"ロードが完了しました";
    }
    
}
- (IBAction)close:(id)sender {
    [self removeFromSuperview];
}

@end
