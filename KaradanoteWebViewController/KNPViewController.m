//
//  KNPViewController.m
//  KaradanoteWebViewController
//
//  Created by akuraru on 10/30/2014.
//  Copyright (c) 2014 akuraru. All rights reserved.
//

#import "KNPViewController.h"
#import "KNPWebViewController.h"
#import "CrayModalWebViewController.h"
#import "CrayNavigationController.h"
#import "AdView.h"

@interface KNPViewController ()
@property (weak, nonatomic) IBOutlet UITextField *urlStringField;

@end

@implementation KNPViewController


- (IBAction)openWebView:(id)sender {
    
    KNPWebViewController *web = [KNPWebViewController showFromViewController:self URL:[self urlString] cache:NO];
//    UIView *dialog = [[UIView alloc] initWithFrame:UIScreen.mainScreen.bounds];
//    dialog.backgroundColor = [UIColor blueColor];
//    web.dialogView = dialog;
    web.navigationBarHidden = NO;
    web.navigationViewShow = YES;
    web.closeButtonImage = [[UIImage imageNamed:@"back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    web.backButtonActiveImage = [[UIImage imageNamed:@"browser_back_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    web.backButtonImage = [[UIImage imageNamed:@"browser_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    web.forwardButtonActiveImage = [[UIImage imageNamed:@"browser_go_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    web.forwardButtonImage = [[UIImage imageNamed:@"browser_go"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    web.adView = [[NSBundle mainBundle] loadNibNamed:@"AdView" owner:self options:nil][0];
    [web setData:@{
        @"days" : @"150",
        @"date" : @"2010-10-10",
    }];
    web.dismiss = ^{
        NSLog(@"close");
    };
}

- (IBAction)openCrayModal:(id)sender {
    [CrayModalWebViewController showFromViewController:self URL:[self urlString]];
}

- (NSString *)urlString {
    NSString *urlString = self.urlStringField.text;
    return (urlString.length != 0) ? urlString : @"http://app.karadanote.jp/";
}
@end
