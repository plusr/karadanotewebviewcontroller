//
//  AdView.h
//  KaradanoteWebViewController
//
//  Created by hakozaki on 2015/10/28.
//  Copyright © 2015年 akuraru. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdView : UIView <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *loadLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@end
