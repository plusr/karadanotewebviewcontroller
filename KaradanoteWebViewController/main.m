//
//  main.m
//  KaradanoteWebViewController
//
//  Created by akuraru on 10/30/2014.
//  Copyright (c) 2014 akuraru. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KNPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KNPAppDelegate class]));
    }
}
