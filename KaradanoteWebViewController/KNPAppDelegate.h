//
//  KNPAppDelegate.h
//  KaradanoteWebViewController
//
//  Created by CocoaPods on 10/30/2014.
//  Copyright (c) 2014 akuraru. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KNPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
